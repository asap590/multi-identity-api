package com.asap.mi.dao;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@NoArgsConstructor
@Data
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String fistName;

    @Column
    private String lastName;

    @Column
    @ElementCollection(targetClass=Integer.class)
    private List<Long> rulesId;

}
