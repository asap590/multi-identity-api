package com.asap.mi.web;

import com.asap.mi.dao.PersonRepository;
import com.asap.mi.dto.PersonInfoDto;
import com.asap.mi.service.PersonRuleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/person")
@AllArgsConstructor
public class PersonController {

    private final PersonRuleService personRuleService;

    @GetMapping("/{id}")
    public PersonInfoDto getPersonData(@PathVariable("id") Long id) {
        return personRuleService.getPersonRules(id);
    }
}
