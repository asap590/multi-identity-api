package com.asap.mi.service;

import com.asap.mi.dao.Person;
import com.asap.mi.dao.PersonRepository;
import com.asap.mi.dto.PersonInfoDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PersonRuleService {

    private final PersonRepository personRepository;

    public PersonInfoDto getPersonRules(Long personId) {
        return personRepository.findById(personId)
                .map(p -> PersonInfoDto.builder()
                .firstName(p.getFistName())
                .lastName(p.getFistName())
                .id(p.getId())
                .rules(p.getRulesId())
                 .build()).orElse(new PersonInfoDto());
    }

}
